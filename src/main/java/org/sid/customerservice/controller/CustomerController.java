package org.sid.customerservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CustomerController {

    @GetMapping("/customer")
    public String getCustomer() {
        return "hello Amira in the devops world, this's our first project devops";
    }

    @GetMapping("/produit")
    public String getProduit() {
        return "hello sofiene in the devops world, this's our first project devops";
    }
}
