package org.sid.customerservice;

import org.sid.customerservice.dao.Customer;
import org.sid.customerservice.dao.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;


@SpringBootApplication

public class CustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args);
    }
    @Bean
    CommandLineRunner start(CustomerRepository customerRepository , RepositoryRestConfiguration restConfigurer){
        return args -> {

            restConfigurer.exposeIdsFor(Customer.class);
            customerRepository.save(new Customer(null , "ENSET" , "sofiene@gmail.com"));
            customerRepository.save(new Customer(null , "ESPRIT" , "sofiene2@gmail.com"));
            customerRepository.save(new Customer(null , "Faculte Monastir" , "sofiene3@gmail.com"));
            customerRepository.save(new Customer(null , "Ecole Hzag" , "sofiene4@gmail.com"));
            customerRepository.findAll().forEach(System.out::println);
        };
    }

}
